-- Dynamiczne interakcje - wysyłanie informacji do klienta --

local interactions = { "PRZYWITAJ", "SLAP", "PLACEHOLDER3", "PLACEHOLDER4", "AVENTEZ" }
addEvent("getInteractions", true)
addEventHandler("getInteractions", root, function()
    triggerClientEvent(client, "setRemoteInteractionList", client, interactions)
end
)

-- Dynamiczne interakcje - odpowiedź na wybraną interakcję i logika --

addEvent("runInteraction", true)
addEventHandler("runInteraction", root, function(typ, target)
    if typ == "PRZYWITAJ" then 
        outputChatBox("Gracz o nicku "..getPlayerName(client).. " przywitał Cię!", target)
        outputChatBox("Przywitałeś gracza o nicku "..getPlayerName(target).. "!", client)
    elseif typ == "SLAP" then
        outputChatBox("Gracz o nicku "..getPlayerName(client).. " uderzył Cię!", target)
        outputChatBox("Uderzyłeś gracza o nicku "..getPlayerName(target).. "!", client)
        setElementHealth(target, getElementHealth(target) - 10)
        local x, y, z = getElementPosition(target)
        setElementPosition(target, x, y, z + 4)
    elseif typ == "PLACEHOLDER3" then
        -- LOGIKA 3 INTERAKCJI --
    end
end
)