-- DEBUG MODE --

setDevelopmentMode(true)

-- Tablica elementów do renderingu i interakcji --

local interactionElements = {}

-- Obsługa przycisków klawiatury --

local interactedElement = nil
local isInteractionKeyPressed = false
addEventHandler("onClientResourceStart", root,
    function()
        bindKey("1", "down", getPlayerInFrontOfMe)
        bindKey("1", "up", interactionKeyReleased)
        triggerServerEvent("getInteractions", localPlayer)
    end
)
function interactionKeyReleased()
    isInteractionKeyPressed = false
    interactedElement = nil
    showCursor(false)
end

-- Front-end rendering --

local screenW, screenH = guiGetScreenSize()
addEventHandler("onClientRender", root, function()
        local countInteractions = #interactionElements
        if isInteractionKeyPressed and interactedElement == nil then
            dxDrawCircle(screenW * 0.5, screenH*0.4, 3, 0, 360, tocolor(255, 0, 0))
        end
        if interactedElement and isInteractionKeyPressed then 
            dxDrawRectangle(screenW * 0.4505, screenH * 0.3463, screenW * 0.0984, screenH * 0.0259, tocolor(179, 179, 179, 211), false) -- Header --
            dxDrawRectangle(screenW * 0.4505, screenH * 0.3463, screenW * 0.0984, screenH * (0.045 + countInteractions * 0.0333), tocolor(0, 0, 0, 190), false) -- Background --
            dxDrawText("Interakcja z "..getPlayerName(interactedElement), screenW * 0.4505, screenH * 0.3463, screenW * 0.5490, screenH * 0.3722, tocolor(255, 255, 255, 255), 1.00, "default", "center", "center", false, false, false, false, false) -- Header text --
            for i, interaction in ipairs(interactionElements) do 
                dxDrawRectangle(screenW * interaction["x"], screenH * interaction["y"], screenW * interaction["width"], screenH * interaction["height"], tocolor(34, 171, 210, 219), false) -- Dynamiczne tworzenie przycisku --
                dxDrawText(interaction["name"], screenW * interaction["x"], screenH * interaction["y"], screenW * interaction["textWidth"], screenH * interaction["textHeight"], tocolor(255, 255, 255, 255), 1.00, "default", "center", "center", false, false, false, false, false) -- Dynamiczne tworzenie przycisku --
            end
        end
    end
)

-- Obsługa front-endu --

addEventHandler ("onClientClick", root, function(button) 
    if interactedElement and button == "left" then
        for i, interaction in ipairs(interactionElements) do
            if isMouseInPosition(screenW * interaction["x"], screenH * interaction["y"], screenW * interaction["width"], screenH * interaction["height"]) then
                triggerServerEvent("runInteraction", localPlayer, interaction["name"], interactedElement)
                interactedElement = nil
                showCursor(false)
                break
            end
        end
    end
end
)

-- Wykrywanie gracza przed nami --

function getPlayerInFrontOfMe()
    isInteractionKeyPressed = true
    local x, y, z = getElementPosition(localPlayer)
    local rotZ = (360 - getPedCameraRotation(localPlayer))
    local x1, y1 = getPointFromDistanceRotation(x, y, 1.5, -rotZ-10)
    local x2, y2 = getPointFromDistanceRotation(x, y, 1.5, -rotZ+10)
    local x3, y3 = getPointFromDistanceRotation(x, y, 0, -rotZ+180)
    local tempCol = createColPolygon(x, y, x1, y1, x2, y2, x3, y3)
    local nearbyPlayers = getElementsWithinRangeEx(x, y, z, 5, "player")
    for i, d in pairs(nearbyPlayers) do
        local pX, pY, pZ = getElementPosition(i)
        local pRotX, pRotY, pRotZ = getElementRotation(i)
        local zDiff = math.abs(z - pZ)
        local object = createObject(3003,pX,pY,pZ) -- obejście do sprawdzenia czy gracz jest w ColShapie (po stronie klienta nie aktualizuje się hit do póty gracz nie wykona ruchu w ColShapie) --
        local incol = isElementWithinColShape(object,tempCol)
        destroyElement(object)
        if zDiff < 4 and incol and interactedElement == nil and i ~= localPlayer then
            getAvailableInteractions()
            interactedElement = i
            showCursor(true)
            break
        end
    end
    destroyElement(tempCol)
end

-- Pobieranie dostępnych informacji o interakcjach z serwera do front-endu i back-ednu --

local remoteInteractionList = {}
addEvent("setRemoteInteractionList", true)
addEventHandler("setRemoteInteractionList", localPlayer, function(list)
    remoteInteractionList = list
end
)
function getAvailableInteractions()
    local width = 0.0880
    local height = 0.0241
    local textWidth = 0.5437
    local textHeight = 0.378
    local x = 0.4557
    local y = 0.3522
    local yOffset = 0.0333
    for i, interaction in ipairs(remoteInteractionList) do
        interactionElements[i] = {}
        interactionElements[i]["name"] = interaction
        interactionElements[i]["x"] = x
        interactionElements[i]["y"] = y + (i * yOffset)
        interactionElements[i]["width"] = width
        interactionElements[i]["height"] = height
        interactionElements[i]["textWidth"] = textWidth
        interactionElements[i]["textHeight"] = textHeight + (i * yOffset)
    end
end

-- Own useful functions --

function getElementsWithinRangeEx(x, y, z, dist, element)
    local nearbyPlayers = getElementsWithinRange(x, y, z, dist, element)
    local sortClosest = {}
    local x, y, z = getElementPosition(localPlayer)
    for i, p in ipairs(nearbyPlayers) do
        local x2, y2, z2 = getElementPosition(p)
        local d = getDistanceBetweenPoints3D(x, y, z, x2, y2, z2)
        sortClosest[p] = math.ceil(d, 1)
    end
    table.sort(sortClosest)
    return sortClosest
end

-- Useful functions --

function getPointFromDistanceRotation(x, y, dist, angle)
    local a = math.rad(90 - angle);
    local dx = math.cos(a) * dist;
    local dy = math.sin(a) * dist;
    return x+dx, y+dy;
end

function isMouseInPosition(x, y, width, height)
    if (not isCursorShowing()) then
        return false
    end
    local sx, sy = guiGetScreenSize ( )
    local cx, cy = getCursorPosition ( )
    local cx, cy = ( cx * sx ), ( cy * sy )
    return ( ( cx >= x and cx <= x + width ) and ( cy >= y and cy <= y + height ) )
end